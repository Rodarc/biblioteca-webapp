# Biblioteca WebApp

Proyecto creado usando [Create React App](https://github.com/facebook/create-react-app).

## Requerimientos

Instalar:
- Node.js
- Yarn

## Preparación

Clonar el respositorio.

Ingresar a la carpeta del repositorio, instalar los requerimientos del proyecto del archivo package.json:
```
yarn install
```

## Ejecucion
Iniciar la aplicación;
```
yarn start
```

Para ver en el navegador abrir la url:


[http://localhost:3000](http://localhost:3000)
