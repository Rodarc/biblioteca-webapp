import React, {Component} from 'react';
import fetchLogin from '../session/actions';
import { Link, Redirect } from 'react-router-dom';
//import './index.css';


class Nav extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="App-nav">
                <Link className= "App-mainbutton" to="/">Biblioteca</Link>
                <Link className= "App-linkbutton" to="/library/">Libros</Link>
                <Link className= "App-linkbutton" to="/">Autores</Link>
                <Link className= "App-linkbutton" to="/login">Logout</Link>
            </div>
        );
    }
}

export default Nav;
