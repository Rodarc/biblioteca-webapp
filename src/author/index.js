import React, {Component} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { fetchAuthorGet, fetchAuthorPost } from './actions';
import { fetchAuthorsGet } from '../library/actions';
import { bindActionCreators } from 'redux';
import uuid from 'uuid';
import {Link, Redirect} from 'react-router-dom';
import Nav from '../nav/index';
import './index.css';

class AuthorRegister extends Component {
    constructor(props){
        super(props);
        this.state = { firstname: '', lastname: '', country: '', datebirth: '', errors: null, change: false};
        this.handleSubmit = this.handleSubmit.bind(this);
        //necesito la lista de autores con su respectivo id, debo pasar el id
    }
    isValid() {
        return (this.state.firstname && this.state.lastname && this.state.country && this.state.datebirth);
    }
    handleSubmit(event){
        let fd = new FormData();
        fd.append('first_name', this.state.firstname);
        fd.append('last_name', this.state.lastname);
        fd.append('country', this.state.country);
        fd.append('date_birth', this.state.datebirth);
        this.props.fetchAuthorPost('/library/author', this.props.token, fd);
        //this.props.fetchBookPost('/library/book', this.props.token, this.state.title, this.state.cover, this.state.date_publication, this.state.edition, this.state.number_copies, this.state.author);
        this.setState({ errors: [], change: true });
        event.preventDefault();
    }
    render(){
        if (!this.props.token){
            return <Redirect to = '/login'/>;
        }
        if(this.state.change){
            return <Redirect to = '/library' push/>;
        }
        return (
            <div className="BookRegister">
                <Nav></Nav>
                    <div className="Actions">
                        <Link className="App-linkbutton" to="/library">Volver</Link>
                    </div>
                <div className="BookInfo">
                    <div className="RightInfo">
                    <form className="login100-form" onSubmit={ this.handleSubmit }>
                        <div className="wrap-input100 validate-input">
                            Nombre:
                            <input placeholder="Nombre" className="App-inputText" type="text" value={this.state.firstname} onChange={(event) => this.setState({firstname: event.target.value})}/>
                        </div>

                        <div className="wrap-input100 validate-input">
                            Apellido:
                            <input placeholder="Apellido" className="App-inputText" type="text" value={this.state.lastname} onChange={(event) => this.setState({lastname: event.target.value})}/>
                        </div>
                        <div className="wrap-input100 validate-input">
                            Pais:
                            <input placeholder="Pais" className="App-inputText" type="text" value={this.state.country} onChange={(event) => this.setState({country: event.target.value})}/>
                        </div>
                        
                        <div className="wrap-input100 validate-input">
                            Fecha de Nacimiento:
                            <input placeholder="Fecha de nacimiento" className="App-inputText" type="date" value={this.state.datebirth} onChange={(event) => this.setState({datebirth: event.target.value})}/>
                        </div>
                        <div className="container-login100-form-btn">
                            <input className="App-button" type="submit" value="Agregar autor" disabled={!this.isValid()}  />
                        </div>
                    </form>

                </div>
                </div>
            </div>
        );

    }

}


AuthorRegister = connect(
  s => ({
    token: s.session.token,
  }),
  d => bindActionCreators({ fetchAuthorPost }, d),
)(AuthorRegister);

export {AuthorRegister};
export default AuthorRegister;
