import * as types from './actions'

const defaultGetState = {
  errors: {},
  data: [],
  api: null
};


const authorReducer = (state = defaultGetState, action) => {
  switch (action.type) {
    case types.FETCH_GET_FETCHING:
      return {
        ...state,
        status: 'loading',
      };
    case types.FETCH_GET_SUCCESS:
      return {
        ...state,
        status: 'done',
        data: action.data,
        api: action.api,
      };
    case types.FETCH_GET_ERROR:
      return {
        ...state,
        status: 'error',
        errors: action.errors,
        data: [],
      };
    case types.FETCH_POST_FETCHING:
      return {
        ...state,
        status: 'loading',
      };
    case types.FETCH_POST_SUCCESS:
      return {
        ...state,
        status: 'done',
        data: action.data,
        api: action.api,
      };
    case types.FETCH_POST_ERROR:
      return {
        ...state,
        status: 'error',
        errors: action.errors,
        data: [],
      };
    case types.FETCH_PATCH_FETCHING:
      return {
        ...state,
        status: 'loading',
      };
    case types.FETCH_PATCH_SUCCESS:
      return {
        ...state,
        status: 'done',
        data: action.data,
        api: action.api,
      };
    case types.FETCH_PATCH_ERROR:
      return {
        ...state,
        status: 'error',
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
};

export default authorReducer;


