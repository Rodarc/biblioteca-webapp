import { combineReducers } from 'redux';

import sessionReducer from '../session/reducers';
import {booksReducer, authorsReducer} from '../library/reducers';
import bookReducer from '../book/reducers';

const rootReducer = combineReducers({
  session: sessionReducer,
  books: booksReducer,
  authors: authorsReducer,
  book: bookReducer,
});

export default rootReducer;