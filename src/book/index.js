import React, {Component} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { fetchBookGet, fetchBookPost, fetchBookPatch, fetchBookDelete } from './actions';
import { fetchAuthorsGet } from '../library/actions';
import { bindActionCreators } from 'redux';
import uuid from 'uuid';
import {Link, Redirect} from 'react-router-dom';
import { fstat } from 'fs';
import Nav from '../nav/index';
import './index.css';

class BookEdit extends Component {
    constructor(props){
        super(props);
        this.state = { title: '', date_publication: '', edition: '', number_copies: '', author: '', errors: null, cover: null, change: false};
        this.handleSubmit = this.handleSubmit.bind(this);
        //necesito la lista de autores con su respectivo id, debo pasar el id
    }
    componentDidMount() {
        //this.props.fetchAuthorsGet('/library/author', this.props.token);
        this.props.fetchBookGet('/library/book', this.props.token, this.props.match.params.id);
    }
    isValid() {
        return (this.state.title || this.state.date_publication || this.state.edition || this.state.number_copies || this.state.author || this.state.cover!=null);
    }
    handleSubmit(event){
        //bool para cada parametro, para asi enivar solo los que necesito, cada bool se controla lo que se renderiza
        let fd = new FormData();
        if(this.state.title && this.state.title !== this.props.book.title){
            fd.append('title', this.state.title);
        }
        if(this.state.cover!=null && this.state.cover !== this.props.book.cover){
            fd.append('cover', this.state.cover);
        }
        if(this.state.date_publication && this.state.date_publication !== this.props.book.date_publication){
            fd.append('date_publication', this.state.date_publication);
        }
        if(this.state.edition && this.state.edition !== this.props.book.edition){
            fd.append('edition', this.state.edition);
        }
        if(this.state.number_copies && this.state.number_copies !== this.props.book.number_copies){
            fd.append('number_copies', this.state.number_copies);
        }
        if(this.state.author && this.state.author !== this.props.book.author){
            fd.append('author', this.state.author);
        }
        this.props.fetchBookPatch('/library/book', this.props.token, this.props.match.params.id, fd);
        this.setState({ errors: [] });
        event.preventDefault();
    }
    render(){
        if (!this.props.token){
            return <Redirect to = '/login'/>;
        }

        const data = this.props.book;
        const authors = this.props.authors;
        if(!authors.length){
            return (
            <div className="container-login100">
                <div className="column wrap-login100">
                    no hay authores
                </div>
            </div>

            )
        }
        return (
            <div className="BookEdit">
                <Nav></Nav>
                <div className="Actions">
                </div>
                <div className="BookInfo">
                    <form className="login100-form" onSubmit={ this.handleSubmit }>
                        <div className="LeftInfo">
                            <img src={this.state.cover != null ? this.state.cover : data.cover} alt="Cover" widht="200" height= "250" ></img><br></br>
                            <input className="App-inputFile" placeholder="File" type="file" value={this.state.file} onChange={(event) => this.setState({cover: event.target.files[0]})}/>
                        </div>
                        <div className="RightInfo">
                            <div className="wrap-input100 validate-input">
                                Título: 
                                <input placeholder="Titulo" className="App-inputText" type="text" value={this.state.title ? this.state.title : data.title} onChange={(event) => this.setState({title: event.target.value})}/>
                            </div>
                            
                            <div className="wrap-input100 validate-input">
                                Fecha de publicación: 
                                <input placeholder="Fecha de publicacion" className="App-inputText" type="date" value={this.state.date_publication ? this.state.date_publication : data.date_publication} onChange={(event) => this.setState({date_publication: event.target.value})}/>
                            </div>
                            <div className="wrap-input100 validate-input">
                                Edición: 
                                <input placeholder="Edicion" className="App-inputText" type="number" min="0" value={this.state.edition ? this.state.edition : data.edition} onChange={(event) => this.setState({edition: event.target.value})}/>
                            </div>
                            <div className="wrap-input100 validate-input">
                                Número de copias:
                                <input placeholder="Número de copias" className="App-inputText" type="number" min="0" value={this.state.number_copies ? this.state.number_copies : data.number_copies} onChange={(event) => this.setState({number_copies: event.target.value})}/>
                            </div>
                            <div className="wrap-input100 validate-input">
                                Autor:
                                <select className="App-inputText" value={this.state.author ? this.state.author : data.author} onChange={(event) => this.setState({author: event.target.value})}>
                                    {authors.length > 0 && (authors.map(a => (
                                    <option key={uuid()} value={a.id}>{a.first_name} {a.last_name}</option>
                                    )))}
                                </select>
                            </div>
                            <div className="container-login100-form-btn">
                                <input className="App-button" type="submit" value="Guardar libro" disabled={!this.isValid()}/>
                                <Link className="App-linkbutton" to={"/library/books/" + data.id}>Cancelar</Link>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        );
    }
}


class BookRegister extends Component {
    constructor(props){
        super(props);
        this.state = { title: '', date_publication: '', edition: '', number_copies: '', author: '', errors: null, cover: null, change: false};
        this.handleSubmit = this.handleSubmit.bind(this);
        //necesito la lista de autores con su respectivo id, debo pasar el id
    }
    isValid() {
        return (this.state.title && this.state.date_publication && this.state.edition && this.state.number_copies && this.state.author && this.state.cover != null);
    }
    handleSubmit(event){
        let fd = new FormData();
        fd.append('title', this.state.title);
        fd.append('cover', this.state.cover);
        fd.append('date_publication', this.state.date_publication);
        fd.append('edition', this.state.edition);
        fd.append('number_copies', this.state.number_copies);
        fd.append('author', this.state.author);
        this.props.fetchBookPost('/library/book', this.props.token, fd);
        //this.props.fetchBookPost('/library/book', this.props.token, this.state.title, this.state.cover, this.state.date_publication, this.state.edition, this.state.number_copies, this.state.author);
        this.setState({ errors: [], change: true });
        event.preventDefault();
    }
    render(){
        if (!this.props.token){
            return <Redirect to = '/login'/>;
        }
        if(this.state.change){
            return <Redirect to = '/library' push/>;
        }
        const authors = this.props.authors;

        return (
            <div className="BookRegister">
                <Nav></Nav>
                    <div className="Actions">
                        <Link className="App-linkbutton" to="/library">Volver</Link>
                    </div>
                <div className="BookInfo">
                    <div className="RightInfo">
                    <form className="login100-form" onSubmit={ this.handleSubmit }>
                        <div className="wrap-input100 validate-input">
                            Título:
                            <input placeholder="Titulo" className="App-inputText" type="text" value={this.state.title} onChange={(event) => this.setState({title: event.target.value})}/>
                        </div>
                        
                        <div className="wrap-input100 validate-input">
                            Fecha de publicación:
                            <input placeholder="Fecha de publicacion" className="App-inputText" type="date" value={this.state.date_publication} onChange={(event) => this.setState({date_publication: event.target.value})}/>
                        </div>
                        <div className="wrap-input100 validate-input">
                            Edición:
                            <input placeholder="Edicion" className="App-inputText" type="number" min="0" value={this.state.edition} onChange={(event) => this.setState({edition: event.target.value})}/>
                        </div>
                        <div className="wrap-input100 validate-input">
                            Número de copias:
                            <input placeholder="Número de copias" className="App-inputText" type="number" min="0" value={this.state.number_copies} onChange={(event) => this.setState({number_copies: event.target.value})}/>
                        </div>
                            <div className="wrap-input100 validate-input">
                                Autor:
                                <select className="App-inputText" value={this.state.author} onChange={(event) => this.setState({author: event.target.value})}>
                                    {authors.length > 0 && (authors.map(a => (
                                    <option key={uuid()} value={a.id}>{a.first_name} {a.last_name}</option>
                                    )))}
                                </select>
                            </div>
                        <div className="wrap-input100 validate-input">
                            Portada:
                            <input placeholder="File" type="file" value={this.state.file} onChange={(event) => this.setState({cover: event.target.files[0]})}/>
                            <span className="focus-input100"></span>
                        </div>

                        <div className="container-login100-form-btn">
                            <input className="App-button" type="submit" value="Agregar libro" disabled={!this.isValid()}  />
                        </div>
                    </form>

                </div>
                </div>
            </div>
        );

    }

}

class BookView extends Component {
    constructor(props){
        super(props);
        this.handleEliminar = this.handleEliminar.bind(this);
    }
    componentDidMount() {
        this.props.fetchBookGet('/library/book', this.props.token, this.props.match.params.id);
    }
    handleEliminar(event){
        //elimnar 
        this.props.fetchBookDelete('/library/book', this.props.token, this.props.match.params.id);
        event.preventDefault();
        this.props.history.push('/library');//igual que redirect
        //y redireccionar
        //con push tal vez, probar
    }
    render(){
        if (!this.props.token){
            return <Redirect to = '/login'/>;
        }

        const data = this.props.book;
        if(!data){//existe? vacio
            return (
                <div className="container-login100">
                    <div className="column wrap-login100">
                        <h3>No hay datos del libro</h3>
                    </div>
                </div>
            );
        }
        else{
            return (
                <div className="BookView">
                    <Nav></Nav>
                    <div className="Actions">
                        <button className="App-button" type="button" onClick={this.handleEliminar}> Eliminar</button>
                        <Link className="App-linkbutton" to={"/library/books/edit/" + data.id}>Editar</Link>
                        <Link className="App-linkbutton" to="/library">Volver</Link>

                    </div>
                    <div className="BookInfo">
                        <div className="LeftInfo">
                            <img src={data.cover} alt="Cover"></img>
                        </div>
                        <div className="RightInfo">
                            <h3>Título: {data.title}</h3>
                            <p>Autor: {data.author}</p>
                            <p>Fecha de publicación: {data.date_publication}</p>
                            <p>Edición: {data.edition}</p>
                            <p>Número de copias: {data.number_copies}</p>
                        </div>
                    </div>
                </div>
            );

        }

    }

}

BookView = connect(
  s => ({
    token: s.session.token,
    book: s.book.data,
  }),
  d => bindActionCreators({ fetchBookGet, fetchBookDelete }, d),
)(BookView);

BookRegister = connect(
  s => ({
    token: s.session.token,
    authors: s.authors.data,
    //book: s.book.data,
  }),
  d => bindActionCreators({ fetchBookPost, fetchAuthorsGet }, d),
)(BookRegister);

BookEdit = connect(
  s => ({
    token: s.session.token,
    book: s.book.data,
    authors: s.authors.data,
  }),
  d => bindActionCreators({ fetchBookGet, fetchBookPatch, fetchAuthorsGet}, d),
)(BookEdit);

export {BookRegister, BookView, BookEdit};
export default BookView;