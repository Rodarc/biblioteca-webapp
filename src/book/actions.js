import api from '../api/index';

export const FETCH_GET_SUCCESS = 'FETCH_GET_SUCCESS';
export const FETCH_GET_FETCHING = 'FETCH_GET_FETCHING';
export const FETCH_GET_ERROR = 'FETCH_GET_ERROR';

export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FETCHING = 'FETCH_POST_FETCHING';
export const FETCH_POST_ERROR = 'FETCH_POST_ERROR';

export const FETCH_PATCH_SUCCESS = 'FETCH_PATCH_SUCCESS';
export const FETCH_PATCH_FETCHING = 'FETCH_PATCH_FETCHING';
export const FETCH_PATCH_ERROR = 'FETCH_PATCH_ERROR';

export const FETCH_DELETE_SUCCESS = 'FETCH_DELETE_SUCCESS';
export const FETCH_DELETE_FETCHING = 'FETCH_DELETE_FETCHING';
export const FETCH_DELETE_ERROR = 'FETCH_DELETE_ERROR';

function fetchGetRequest() {
  return {
    type: FETCH_GET_FETCHING,
  };
}

function fetchGetSuccess(data) {
  return {
    type: FETCH_GET_SUCCESS,
    data,
  };
}

function fetchGetErrors(e) {
  return {
    type: FETCH_GET_ERROR,
    errors: e,
  }
}

function fetchPostRequest() {
  return {
    type: FETCH_POST_FETCHING,
  };
}

function fetchPostSuccess(data) {
  return {
    type: FETCH_POST_SUCCESS,
    data,
  };
}

function fetchPostErrors(e) {
  return {
    type: FETCH_POST_ERROR,
    errors: e,
  }
}

function fetchPatchRequest() {
  return {
    type: FETCH_PATCH_FETCHING,
  };
}

function fetchPatchSuccess(data) {
  return {
    type: FETCH_PATCH_SUCCESS,
    data,
  };
}

function fetchPatchErrors(e) {
  return {
    type: FETCH_PATCH_ERROR,
    errors: e,
  }
}

function fetchDeleteRequest() {
  return {
    type: FETCH_DELETE_FETCHING,
  };
}

function fetchDeleteSuccess(data) {
  return {
    type: FETCH_DELETE_SUCCESS,
    data,
  };
}

function fetchDeleteErrors(e) {
  return {
    type: FETCH_DELETE_ERROR,
    errors: e,
  }
}

export function fetchBookGet(url, token, id) {
  return (dispatch) => {
    dispatch(fetchGetRequest());
    api.get(`/library/book/${id}/`,
        {headers: {Authorization: `Token ${token}`},}
  ).then((r) => {
      dispatch(fetchGetSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchGetErrors(e));
    });
  };
}

export function fetchBookPost(url, token, fd) {
  return (dispatch) => {
    dispatch(fetchPostRequest());
    api.post(`/library/book/`, fd, 
        {headers: {Authorization: `Token ${token}`, 'content-type': 'multipart/form-data'},}
  ).then((r) => {
      dispatch(fetchPostSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchPostErrors(e));
    });
  };
}

export function fetchBookPatch(url, token, id,  fd) {
  return (dispatch) => {
    dispatch(fetchPatchRequest());
    api.patch(`/library/book/${id}/`, fd, 
        {headers: {Authorization: `Token ${token}`, 'content-type': 'multipart/form-data'},}
  ).then((r) => {
      dispatch(fetchPatchSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchPatchErrors(e));
    });
  };
}

export function fetchBookDelete(url, token, id) {
  return (dispatch) => {
    dispatch(fetchDeleteRequest());
    api.delete(`/library/book/${id}/`, 
        {headers: {Authorization: `Token ${token}`},}
  ).then((r) => {
      dispatch(fetchDeleteSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchDeleteErrors(e));
    });
  };
}