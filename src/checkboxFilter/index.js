import React, {Component} from 'react';
import uuid from 'uuid';

class CheckboxItem extends Component{
    constructor(props){
        super(props);
        this.state = {checked: false};//this.props.checked};
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event){
        this.setState({checked: !this.state.checked})
        //this.setState({checked: event.target.checked})
        this.props.onChange(this.props.name, this.state.checked);//que recibe?
    }
    render(){
        return(
            <input type="checkbox"  name={this.props.value} value={this.props.value} checked={this.state.checked} onChange={this.handleChange}></input>
        );
    }

}

class CheckboxFilter extends Component{
    constructor(props){
        super(props);
        this.state = {selected: []}
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }
    handleCheckboxChange(checkboxid, checkedvalue){
        //agregar el checkbos id a selected y con el value cambiar su estatus
        let newselected = [];
        if(!checkedvalue){
            newselected = this.state.selected.concat(checkboxid);
            this.setState({selected: this.state.selected.concat(checkboxid)});
        }
        else{
            newselected = this.state.selected.filter(item => item != checkboxid);
            this.setState({selected: this.state.selected.filter(item => item != checkboxid)});
        }
        //this.setState({[checkboxid]: checkedvalue});

        //this.props.onChange(this.state.selected);//manda un estado anterior
        this.props.onChange(newselected);//llamando al callback
    }
    render(){
        const checkboxes = this.props.checkboxes;
        return (
            <div>
            {checkboxes.map(cb => (
                <div>
                <CheckboxItem key={cb.id} name={cb.id} value={cb.value} onChange={this.handleCheckboxChange} /*checked={this.state[cb.id]}*/></CheckboxItem>{cb.first_name}<br></br>
                </div>
            )) }
            </div>
        );
    }
}

export { CheckboxFilter, CheckboxItem };