import api from '../api/index';

export const LOGOUT = 'LOGOUT';
export const LOGIN_FETCHING = 'LOGIN_FETCHING';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const REGISTER_FETCHING = 'REGISTER_FETCHING';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

function loginRequest(username){
    return {type: LOGIN_FETCHING, username, };
}

function loginSuccess(token){
    return {type: LOGIN_SUCCESS, token, };
}

function loginErrors(errors){
    return {type: LOGIN_ERROR, errors, };
}

function logoutAction(){
    return {type: LOGIN_ERROR, errors: [], };
}

function registerRequest(){
    return {type: REGISTER_FETCHING};
}

function registerSuccess(data){
    return {type: REGISTER_SUCCESS, data, };
}

function registerErrors(e){
    return {type: REGISTER_ERROR, errors: e, };
}

export function fetchLogin(username, password){
    return (dispatch) => {
        dispatch(loginRequest(username));
        api.post('/accounts/login/', {username, password}).then(
            (r) => {dispatch(loginSuccess(r.data.token));}).catch(
                (e) => {} );
    };
}

export function fetchRegister(fd) {
  return (dispatch) => {
    dispatch(registerRequest());
    api.post(`/accounts/register/`, fd, 
  ).then((r) => {
      dispatch(registerSuccess(r.data));
    }).catch((e) => {
      dispatch(registerErrors(e));
    });
  };
}


export default fetchLogin;