import * as types from './actions';

const defaultSessionState = {
  isLoading: false,
  errors: {},
  success: false,
  token: '',
};

const sessionReducer = (state = defaultSessionState, action) => {
  switch (action.type) {
    case types.LOGIN_ERROR:
      return {
        ...state,
        isLoading: false,
        errors: action.errors,
      };
    case types.LOGIN_FETCHING:
      return {
        ...state,
        isLoading: true,
        username: action.username,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        success: true,
        isLoading: false,
        token: action.token,
        errors: []
      };
    case types.LOGOUT:
      return {
        ...state,
        success: true,
        isLoading: false,
        token: '',
        username: '',
        errors: action.errors,
        notification: 0,
      };
    case types.REGISTER_FETCHING:
      return {
        ...state,
        status: 'loading',
      };
    case types.REGISTER_SUCCESS:
      return {
        ...state,
        status: 'done',
        data: action.data,
        api: action.api,
      };
    case types.REGISTER_ERROR:
      return {
        ...state,
        status: 'error',
        errors: action.errors,
        data: [],
      };

    default:
      return state;
  }
};

export default sessionReducer;