import React, {Component} from 'react';
import logo from './logo.svg';
import {Login, Register} from './users/index';
import Booksview from './library';
import { BookView, BookRegister, BookEdit } from './book';
import { AuthorRegister } from './author';
import { Provider } from 'react-redux';
import store from './store';
import './App.css';
import {BrowserRouter, Route, Link, Redirect, withRouter, Switch } from 'react-router-dom';

class App extends Component {
  state = {loggued: false};
  inicio(){
    if(this.state.loggued){
      return <Redirect to = '/library'/>
    }
    else{
      return <Redirect to = '/login'/>
    }
  }
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <Provider store={store}>
          <BrowserRouter>
            <Switch>
            <Route path="/login" component = {Login}/>
            <Route path="/register" component = {Register}/>
            <Route path="/library/authors/register" component = {AuthorRegister}/>
            <Route path="/library/books/register" component = {BookRegister}/>
            <Route path="/library/books/edit/:id" component = {BookEdit}/>
            <Route path="/library/books/:id" component = {BookView}/>
            <Route path="/library" component = {Booksview}/>
            <Route path="/" render= {()=>(this.inicio())}/>
            </Switch>
          </BrowserRouter>
          </Provider>
        </header>
      </div>
    );
  }
}

export default App;
