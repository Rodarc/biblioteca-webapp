import api from '../api/index';

export const FETCH_GET_SUCCESS = 'FETCH_GET_SUCCESS';
export const FETCH_GET_FETCHING = 'FETCH_GET_FETCHING';
export const FETCH_GET_ERROR = 'FETCH_GET_ERROR';

export const FETCH_GETA_SUCCESS = 'FETCH_GETA_SUCCESS';
export const FETCH_GETA_FETCHING = 'FETCH_GETA_FETCHING';
export const FETCH_GETA_ERROR = 'FETCH_GETA_ERROR';

function fetchGetRequest() {
  return {
    type: FETCH_GET_FETCHING,
  };
}

function fetchGetSuccess(data) {
  return {
    type: FETCH_GET_SUCCESS,
    data,
  };
}

function fetchGetErrors(e) {
  return {
    type: FETCH_GET_ERROR,
    errors: e,
  }
}

function fetchGetARequest() {
  return {
    type: FETCH_GETA_FETCHING,
  };
}

function fetchGetASuccess(data) {
  return {
    type: FETCH_GETA_SUCCESS,
    data,
  };
}

function fetchGetAErrors(e) {
  return {
    type: FETCH_GETA_ERROR,
    errors: e,
  }
}

export function fetchBooksGet(url, token) {
  return (dispatch) => {
    dispatch(fetchGetRequest());
    api.get('/library/book/',
        {headers: {Authorization: `Token ${token}`},}
  ).then((r) => {
      dispatch(fetchGetSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchGetErrors(e));
    });
  };
}

export function fetchAuthorsGet(url, token) {
  return (dispatch) => {
    dispatch(fetchGetARequest());
    api.get('/library/author/',
        {headers: {Authorization: `Token ${token}`},}
  ).then((r) => {
      dispatch(fetchGetASuccess(r.data));
    }).catch((e) => {
      dispatch(fetchGetAErrors(e));
    });
  };
}

export function fetchBooksSearchGet(url, token, phrasesearch) {
  return (dispatch) => {
    dispatch(fetchGetRequest());
    api.get('/library/book/search/',
        {headers: {Authorization: `Token ${token}`},
        params: {phrase: phrasesearch},
      }
  ).then((r) => {
      dispatch(fetchGetSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchGetErrors(e));
    });
  };
}

export function fetchBooksFilterGet(url, token, initialyear, finalyear, authors, phrase) {
  var parametros =  new URLSearchParams();
  for(var i = 0; i < authors.length; i++){
    parametros.append("authors", authors[i]);
  }
  if(initialyear){
    parametros.append("initialyear", initialyear);
  }
  if(finalyear){
    parametros.append("finalyear", finalyear);
  }
  if(phrase){
    parametros.append("phrase", phrase);
  }
  return (dispatch) => {
    dispatch(fetchGetRequest());
    api.get('/library/book/filter_query/',
        {headers: {Authorization: `Token ${token}`},
        params: parametros,
      }
  ).then((r) => {
      dispatch(fetchGetSuccess(r.data));
    }).catch((e) => {
      dispatch(fetchGetErrors(e));
    });
  };
}

export const AllGets = (url, token) => (dispatch) => {
    dispatch(fetchBooksGet(url, token))
    dispatch(fetchAuthorsGet(url, token))
}