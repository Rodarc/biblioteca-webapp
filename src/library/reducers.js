import * as types from './actions'

const defaultGetState = {
  errors: {},
  data: [],
  api: null
};

const booksReducer = (state = defaultGetState, action) => {
  switch (action.type) {
    case types.FETCH_GET_FETCHING:
      return {
        ...state,
        status: 'loading',
      };
    case types.FETCH_GET_SUCCESS:
      return {
        ...state,
        status: 'done',
        data: action.data,
        api: action.api,
      };
    case types.FETCH_GET_ERROR:
      return {
        ...state,
        status: 'error',
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
};

const authorsReducer = (state = defaultGetState, action) => {
  switch (action.type) {
    case types.FETCH_GETA_FETCHING:
      return {
        ...state,
        status: 'loading',
      };
    case types.FETCH_GETA_SUCCESS:
      return {
        ...state,
        status: 'done',
        data: action.data,
        api: action.api,
      };
    case types.FETCH_GETA_ERROR:
      return {
        ...state,
        status: 'error',
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
};

export {booksReducer, authorsReducer};
export default booksReducer;
