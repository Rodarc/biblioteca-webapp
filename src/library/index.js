import React, {Component} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { fetchBooksGet, fetchBooksSearchGet, fetchBooksFilterGet, fetchAuthorsGet, AllGets } from './actions';
import { bindActionCreators } from 'redux';
import Nav from '../nav/index';
import uuid from 'uuid';
import {Link, Route, Redirect} from 'react-router-dom';
import {CheckboxFilter, CheckboxItem } from '../checkboxFilter';
import './index.css';

class BooksView extends Component {
    constructor(props){
        super(props);
        this.state = {phrase_search: '', authorfilters: [], initialyear:'', finalyear:'', years: []};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCheckboxFilter = this.handleCheckboxFilter.bind(this);
        this.search = this.search.bind(this);
        this.filter = this.filter.bind(this);
    }
    componentDidMount() {
        this.props.AllGets('/library/', this.props.token);
        //estos fallan
        //this.props.fetchAuthorsGet('/library/author', this.props.token);
        //this.props.fetchBooksGet('/library/book', this.props.token);
        var newyears = [];
        for(var i = 1990; i < 2020; i++){
            if(newyears.indexOf(i) === -1){
                newyears.push(i);
            }
        }
        this.setState({years: newyears});
    }

    handleCheckboxFilter(options_selected){
        this.setState({authorfilters: options_selected});
    }

    handleSubmit(event){
        this.setState({ errors: [] });
    }
    search(event){
        //this.props.fetchBooksSearchGet('/library/book/search/', this.props.token, this.state.phrase_search);

        this.props.fetchBooksFilterGet('/library/book/filter_query/', this.props.token, this.state.initialyear, this.state.finalyear, this.state.authorfilters, this.state.phrase_search);
    }
    filter(event){
        if(this.state.initialyear=='' && this.state.finalyear=='' && this.state.authorfilters == []){
            this.props.fetchBooksGet('/library/book', this.props.token);
        }
        else{
            this.props.fetchBooksFilterGet('/library/book/filter_query/', this.props.token, this.state.initialyear, this.state.finalyear, this.state.authorfilters, this.state.phrase_search);
        }
    }
    render(){
        if (!this.props.token){
            return <Redirect to = '/login'/>;
        }
        let data = this.props.books;
        let dataa = this.props.authors;
        if(!data.length){
            data = [];
        }
        if(!dataa.length){
            dataa = []
        }
        return (
            <div className="Library">
                <Nav></Nav>
                <div className="Actions">
                    <input className="App-inputText" placeholder= "Search" type="text" onChange={(event) => this.setState({phrase_search: event.target.value})}></input>
                    <button className="App-button" onClick={this.search}> Buscar </button>
                    <Link className= "App-linkbutton" to="/library/books/register">Agregar Libro</Link>
                    <Link className= "App-linkbutton" to="/library/authors/register">Agregar Autor</Link>
                </div>
                <br></br>
                <div className="Filters">
                    <h2>Filtros</h2>
                    <button className="App-button" onClick={this.filter}> Aplicar </button>
                    <h3>Autores</h3>
                    <CheckboxFilter key="AuthorFilter" checkboxes={dataa} onChange={this.handleCheckboxFilter}></CheckboxFilter>

                    <h3>Periodo</h3>
                    Año inicial
                    <select value={this.state.initialyear ? this.state.initialyear : ''} onChange={(event) => this.setState({initialyear: event.target.value})}>
                        <option key={uuid()} value=''>--</option>
                        {this.state.years.length > 0 && (this.state.years.map(y => (
                        <option key={uuid()} value={y}>{y}</option>
                        )))}
                    </select>
                    Año final 
                    <select value={this.state.finalyear ? this.state.finalyear : ''} onChange={(event) => this.setState({finalyear: event.target.value})}>
                        <option key={uuid()} value=''>--</option>
                        {this.state.years.length > 0 && (this.state.years.map(y => (
                        <option key={uuid()} value={y}>{y}</option>
                        )))}
                    </select>
                </div>
                <div className="Gallery">
                    <h2>Libros</h2>
                    {data.map(b => (
                    <div className="BookItem"key={uuid()}>
                        <div className="Header"key={uuid()}>
                        <img src={b.cover} alt="Cover" width="200" height="250" max-width="200" max-height="250"></img>
                        </div>
                        <div className="Body"key={b.id}>
                        <p key={uuid()}><Link className="Title-link" to={"/library/books/" + b.id}>{b.title}</Link></p>
                        <p key={uuid()}>Autor: {dataa.length > b.author-1 ? dataa[b.author-1].first_name : b.author}</p>
                        <p key={uuid()}>Fecha de publicación: {b.date_publication}</p>
                        <p key={uuid()}>Edición: {b.edition}</p>
                        <p key={uuid()}>Número de copias: {b.number_copies}</p>
                        </div>
                    </div>
                    ))}
                </div>
            </div>
        );
                        //<Route path="/library/books/:id" component = {Bookview}/>

        //}
    }
}

BooksView = connect(
  s => ({
    token: s.session.token,
    books: s.books.data,
    authors: s.authors.data,
  }),
  d => bindActionCreators({ fetchBooksGet, fetchBooksSearchGet, fetchBooksFilterGet, fetchAuthorsGet, AllGets }, d),
)(BooksView);


export default BooksView;

