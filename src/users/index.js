import React, {Component} from 'react';
import {fetchLogin, fetchRegister} from '../session/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect, Link } from 'react-router-dom';
import './index.css';

class Register extends Component {
    constructor(props){
        super(props);
        this.state = { username: '', password: '', firstname: '', lastname:'', email: '', errors: null, redirect: false };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.isValid = this.isValid.bind(this);
    }
    isValid() {
        return (this.state.username && this.state.password && this.state.firstname && this.state.lastname && this.state.email);
    }
    handleSubmit(event){
        let fd = new FormData();
        fd.append('username', this.state.username);
        fd.append('password', this.state.password);
        fd.append('first_name', this.state.firstname);
        fd.append('last_name', this.state.lastname);
        fd.append('email', this.state.email);
        this.props.fetchRegister(fd);
        this.setState({ errors: [] , redirect: true});
        event.preventDefault();
    }
    render(){
        if (this.state.redirect){
            return <Redirect to = '/login/'/>;
        }
        else {
            return (
                <div className="container-login100">
                    <h1 className="Title">Biblioteca</h1>
                    <div className="column wrap-login100">
                        <form className="login100-form" onSubmit={ this.handleSubmit }>
                            <div className="wrap-input100 validate-input">
                                <input placeholder="Usuario" className="App-inputText" type="text" value={this.state.username} onChange={(event) => this.setState({username: event.target.value})}/>
                            <span className="focus-input100"></span>
                            </div>
                            
                            <div className="wrap-input100 validate-input">
                                <input placeholder="Contrasena" className="App-inputText" type="password" value={this.state.password} onChange={(event) => this.setState({password: event.target.value})}/>
                                <span className="focus-input100"></span>
                            </div>
                            <div className="wrap-input100 validate-input">
                                <input placeholder="Nombre" className="App-inputText" type="text" value={this.state.firstname} onChange={(event) => this.setState({firstname: event.target.value})}/>
                            <span className="focus-input100"></span>
                            </div>

                            <div className="wrap-input100 validate-input">
                                <input placeholder="Apellido" className="App-inputText" type="text" value={this.state.lastname} onChange={(event) => this.setState({lastname: event.target.value})}/>
                            <span className="focus-input100"></span>
                            </div>

                            <div className="wrap-input100 validate-input">
                                <input placeholder="Email" className="App-inputText" type="email" value={this.state.email} onChange={(event) => this.setState({email: event.target.value})}/>
                            <span className="focus-input100"></span>
                            </div>

                            <div className="container-login100-form-btn">
                                <input className="App-button" type="submit" value="Registrarse" disabled={!this.isValid()} />
                            </div>
                            <Link className="App-linkbutton" to="/login"> Login </Link>
                            
                        </form>
                    </div>
                </div>
            );
        }
    }
}

class Login extends Component {
    constructor(props){
        super(props);
        this.state = { username: '', password: '', errors: null, redirect: false };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.isValid = this.isValid.bind(this);
    }
    isValid() {
        return (this.state.username && this.state.password);
    }
    handleSubmit(event){
        this.props.fetchLogin(this.state.username, this.state.password);
        this.setState({ errors: [] });
        event.preventDefault();
    }
    render(){
        if (this.props.session.success){
            return <Redirect to = '/library'/>;
        }
        else {
            return (
                <div className="container-login100">
                    <h1 className="Title">Biblioteca</h1>
                    <div className="column wrap-login100">
                        <form className="login100-form" onSubmit={ this.handleSubmit }>
                            <div className="wrap-input100 validate-input">
                                <input placeholder="Usuario" className="App-inputText" type="text" value={this.state.username} onChange={(event) => this.setState({username: event.target.value})}/>
                            <span className="focus-input100"></span>
                            </div>
                            
                            <div className="wrap-input100 validate-input">
                                <input placeholder="Contrasena" className="App-inputText" type="password" value={this.state.password} onChange={(event) => this.setState({password: event.target.value})}/>
                                <span className="focus-input100"></span>
                            </div>
                            <div className="container-login100-form-btn">
                                <input className="App-button" type="submit" value="Iniciar Sesion" disabled={!this.isValid()} />
                            </div>

                            <Link className="App-linkbutton" to="/register"> Registrarse </Link>
                            
                        </form>
                    </div>
                </div>
            );

        }
    }
}

Login = connect(
  s => ({ session: s.session, }),
  d => bindActionCreators({ fetchLogin }, d),
)(Login);

Register = connect(
  s => ({ session: s.session, }),
  d => bindActionCreators({ fetchRegister }, d),
)(Register);

export {Login, Register};
export default Login;